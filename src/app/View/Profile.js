import { useEffect, useState, useContext } from "react";
import { View, Text, Button, TextInput, StyleSheet, ScrollView } from 'react-native';
import Constants from 'expo-constants';
import { UserContext } from "../Context/UserContext";
import ErrorPopup from "./ErrorPopup";

export default function Profile ({ navigation , route }) {
	const { user, updateUser } = useContext(UserContext);
	const [ userData, setUserData ] = useState(null);
	const [ loading, setLoading ] = useState(true);
	useEffect(() => {
        const loadPost = async () => {
            var request = {
                method: 'Post',
                headers: { 
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin' :'*',
					'Authorization': 'Bearer ' + user.userToken
                },
				body: JSON.stringify({ user_name: user.username })
            }
            const response = await fetch(`http://${Constants.expoConfig.extra.apiUrl}:${Constants.expoConfig.extra.apiPort}/user`, request);
            const data = await response.json();
            setUserData(data);
            setLoading(false);
        }
        loadPost();
    }, []);

	return (
  <>
			{loading ? (
				<Text>Loading...</Text>
			) : (
				<View style={styles.container}>
					<ScrollView>
						<View style={styles.aboutBox}>
					<View style={styles.inputContainer}>
						<Text>Email</Text>
						<TextInput
						placeholderTextColor="#003f5c"
						style={styles.input}
						defaultValue={userData.email}
						onChangeText={(text) => {
							userData.email = text;
						}}
						/>
					</View>
					<View style={styles.inputContainer}>
						<Text>first name</Text>
						<TextInput
							placeholderTextColor="#003f5c"
							style={styles.input}
							defaultValue={userData.first_name}
							onChangeText={(text) => {
								userData.first_name = text;
							}}
						/>
					</View>
					<View style={styles.inputContainer}>
						<Text>last name</Text>
						<TextInput
							placeholderTextColor="#003f5c"
							style={styles.input}
							defaultValue={userData.last_name}
							onChangeText={(text) => {
								userData.last_name = text
							}}
						/>
					</View>
					<View style={styles.inputContainer}>
						<Text>student id</Text>
						<TextInput
							placeholderTextColor="#003f5c"
							style={styles.input}
							defaultValue={userData.student_id}
							onChangeText={(text) => {
								userData.student_id = text;
							}}
						/>
					</View>
					<View style={styles.inputContainer}>
						<Text>birthday</Text>
						<TextInput
							placeholderTextColor="#003f5c"
							style={styles.input}
							defaultValue={userData.birthday}
							onChangeText={(text) => {
								userData.birthday = text;
							}}
						/>
					</View>
					<View style={styles.inputContainer}>
						<Text>sex</Text>
						<TextInput
							placeholderTextColor="#003f5c"
							style={styles.input}
							defaultValue={userData.sex}
							onChangeText={(text) => {
								userData.sex = text;
							}}
						/>
					</View>
					<Button
						title="Update"
						onPress={() => {
							var request = {
								method: 'PUT',
								headers: { 
									'Content-Type': 'application/json',
									'Access-Control-Allow-Origin' :'*',
									'Authorization': 'Bearer ' + user.userToken
								},
								body: JSON.stringify(userData)
							}
							fetch(`http://${Constants.expoConfig.extra.apiUrl}:${Constants.expoConfig.extra.apiPort}/user`,request)
							.then(response => {
								if(!response.ok){
									response.json().then(data => {
										ErrorPopup("Error", data.error,
										[{text: 'OK', onPress: () => console.log('OK Pressed')}],
										)
									})
								}
								else {
									navigation.navigate('Main');
								}
							});
						}}
					/>
							</View>
					</ScrollView>
                </View>)
        }
        </>
    );

}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
		backgroundColor: "#faf2cc",
		justifyContent: "flex-start",
		alignItems: "center",
	},
	aboutBox: {
		backgroundColor: "#fff",
		padding: 20,
		borderRadius: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 4,
		elevation: 5,
		alignSelf: "stretch",
	},
	inputContainer: {
		marginBottom: 20,
	},
	input: {
		width: "100%",
		height: 40,
		borderWidth: 1,
		borderColor: "#003f5c",
		borderRadius: 20,
		paddingLeft: 15,
		marginBottom: 10,
	},
});