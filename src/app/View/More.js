import { StyleSheet } from "react-native-web";
import { View, TouchableOpacity, Text } from 'react-native';

export default function More({ navigation, route }) {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>More</Text>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TermCondition')}>
                <Text style={styles.buttonText}>Term & Conditions</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('PrivacySecurity')}>
                <Text style={styles.buttonText}>Privacy & Security</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('About')}>
                <Text style={styles.buttonText}>About Us</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Tips')}>
                <Text style={styles.buttonText}>Tips</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('FAQ')}>
                <Text style={styles.buttonText}>FAQ</Text>
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#faf2cc",
        padding: 20,
    },
    title: {
        fontSize: 24,
        fontWeight: "bold",
        marginBottom: 20,
        color: "#333",
        textAlign: "center",
        textTransform: "uppercase",
        letterSpacing: 2,
    },
    button: {
        backgroundColor: "#007aff",
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 5,
        alignItems: "center",
        marginBottom: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#fff",
    },
});
