import { View, Text, StyleSheet } from "react-native";

export default function Coverage({ navigation, route }) {
    return (
        <View style={styles.container}>
            <View style={styles.section}>
                <Text style={styles.title}>YOUR PLAN BENEFITS</Text>
                <View style={styles.subsection}>
                    <Text style={styles.subtitle}>Health-Care Coverage</Text>
                    <Text style={styles.info}>
                        Prescription drugs & vaccinations
                    </Text>
                    <Text style={styles.info}>Psychologists</Text>
                    <Text style={styles.info}>Physiotherapists</Text>
                    <Text style={styles.info}>Chiropractors</Text>
                    <Text style={styles.info}>Medical equipment</Text>
                    <Text style={styles.info}>Ambulance</Text>
                </View>
                <View style={styles.subsection}>
                    <Text style={styles.subtitle}>Travel Coverage</Text>
                    <Text style={styles.info}>$1,000,000 per lifetime</Text>
                    <Text style={styles.info}>150 days per trip</Text>
                </View>
                <View style={styles.subsection}>
                    <Text style={styles.subtitle}>Vision Coverage</Text>
                    <Text style={styles.info}>Eye exam</Text>
                    <Text style={styles.info}>Eyeglasses & contact lenses</Text>
                    <Text style={styles.info}>Laser eye surgery</Text>
                </View>
                <View style={styles.subsection}>
                    <Text style={styles.subtitle}>Dental Coverage</Text>
                    <Text style={styles.info}>Checkups</Text>
                    <Text style={styles.info}>Cleanings</Text>
                    <Text style={styles.info}>Extractions</Text>
                    <Text style={styles.info}>Fillings</Text>
                    <Text style={styles.info}>Oral surgery</Text>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: "#faf2cc",
    },
    section: {
        marginBottom: 20,
        backgroundColor: "#fff",
        padding: 20,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    title: {
        fontSize: 24,
        fontWeight: "bold",
        marginBottom: 20,
        color: "#333",
        textAlign: "center",
        textTransform: "uppercase",
        letterSpacing: 2,
    },
    subsection: {
        marginBottom: 20,
        paddingLeft: 20,
    },
    subtitleContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 10,
    },
    subtitle: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#666",
        textTransform: "uppercase",
        letterSpacing: 1,
        marginLeft: 10,
    },
    icon: {
        width: 20,
        height: 20,
    },
    info: {
        fontSize: 16,
        marginBottom: 5,
        color: "#777",
    },
    infoClickable: {
        fontSize: 16,
        marginBottom: 5,
        color: "#007aff",
        textDecorationLine: "underline",
    },
});
