import React, { useState, useContext } from "react";
import { TextInput, View, TouchableOpacity, StyleSheet, Text, Image } from "react-native";
import Constants from 'expo-constants';
import { UserContext } from "../Context/UserContext";
import ErrorPopup from "./ErrorPopup";

export default function Register({ navigation, route }) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const { user, updateUser } = useContext(UserContext);
    const handleUserNameChange = (usernameText) => {
        updateUser({ username: usernameText });
    };
    return (
        <View style={styles.container}>
            <Image source={require('../../assets/logo-student-care.png')} style={styles.logo} />
            <Text style={styles.title}>Register</Text>
            <View style={styles.inputContainer}>
                <TextInput
                    placeholder="Enter your username"
                    placeholderTextColor="#777"
                    style={styles.input}
                    onChangeText={(text) => {
                        handleUserNameChange(text);
                    }}
                />
            </View>
            <View style={styles.inputContainer}>
                <TextInput
                    placeholder="Enter your email"
                    placeholderTextColor="#777"
                    style={styles.input}
                    onChangeText={(text) => {
                        setEmail(text);
                    }}
                />
            </View>
            <View style={styles.inputContainer}>
                <TextInput
                    placeholder="Enter your password"
                    placeholderTextColor="#777"
                    style={styles.input}
                    secureTextEntry={true}
                    onChangeText={(text) => {
                        setPassword(text);
                    }}
                />
            </View>
            <TouchableOpacity
                style={styles.button}
                onPress={() => {
					var request = {
						method: 'POST',
						headers: { 
							'Content-Type': 'application/json',
							'Access-Control-Allow-Origin' :'*' 
						},
						body: JSON.stringify({ user_name: user.username, password: password, email: email})
					}
					fetch(`http://${Constants.expoConfig.extra.apiUrl}:${Constants.expoConfig.extra.apiPort}/register`,request)
					.then(response => {
						if(!response.ok){
							response.json().then(data => {
								ErrorPopup("Error", data.error,
								    [{text: 'OK', onPress: () => console.log('OK Pressed')}],
								)
							})
						}
						else {
							response.json().then(data => {
								navigation.navigate('Login')
							})	
						}
					});
				}}
            >
                <Text style={styles.buttonText}>Register</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.button}
                onPress={() => {
                    navigation.navigate('Login')
                }}
            >
                <Text style={styles.buttonText}>Return to Login</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#faf2cc",
        padding: 20,
    },
    title: {
        fontSize: 24,
        fontWeight: "bold",
        marginBottom: 20,
        color: "#333",
        textAlign: "center",
        textTransform: "uppercase",
        letterSpacing: 2,
    },
    inputContainer: {
        marginBottom: 20,
    },
    input: {
        width: 300,
        height: 40,
        borderWidth: 1,
        borderColor: "#007aff",
        borderRadius: 5,
        paddingLeft: 15,
        backgroundColor: "#fff",
    },
    button: {
        backgroundColor: "#007aff",
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 5,
        alignItems: "center",
        marginBottom: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#fff",
    },

    logo: {
        width: 500,
        height: 300,
        resizeMode: 'contain',
        marginBottom: 20,
    },
});
