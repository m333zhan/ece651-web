import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';

export default function PrivacySecurity({ navigation, route }) {
    return (
        <View style={styles.container}>
            <Image source={require('../../assets/logo-student-care.png')} style={styles.logo} />
            <Text style={styles.title}>Privacy & Security</Text>
            <ScrollView style={styles.scrollView}>
                <Text style={styles.text}>
                    SUMMARY{'\n'}
                    {'\n'}
                    We collect personal and activity data to enhance your experience.
                    {'\n'}
                    We collect but do not share your location history.
                    {'\n'}
                    We share your claim data only with your insurer.
                    {'\n'}
                    No ad companies collect data through our service.
                    {'\n'}
                    We take detailed steps to protect your personal information.
                    {'\n'}
                    We store your encrypted data as long as is necessary for policy and
                    claim management.
                    {'\n'}
                    You can request to see, update and/or delete your personal data.
                    {'\n'}
                    Our full privacy policy includes more information, and you can always
                    ask us questions.
                    {'\n'}{'\n'}
                    ASEQ | Studentcare administers the student group benefits plan on
                    behalf of student associations, in order to ensure that students
                    receive the best service possible. The ihaveaplan mobile service
                    provided by ASEQ | Studentcare connects eligible students with their
                    insurance provider for the purpose of easier claim submission.
                    {'\n'}
                    This document describes our current policies and practices with regard
                    to personal information collected and used by us through the
                    Studentcare mobile service application.
                    {'\n'}
                    For further information about our commitment to keeping personal
                    information private and confidential, please see the complete
                    Studentcare Privacy Policy online at www.studentcare.ca/privacy.
                    {'\n'}{'\n'}
                    Managing your data
                    {'\n'}
                    Students can access information relating to them contained in our
                    files, including the existence of the file, by submitting a written
                    and signed request, accompanied by a photocopy of their student I.D.
                    and a stamped, self addressed envelope to:
                    {'\n'}
                    Attention: Information Request
                    {'\n'}
                    ASEQ | Studentcare
                    {'\n'}
                    1200 McGill College Avenue, Suite 2200
                    {'\n'}
                    Montreal, QC  H3B 4G7
                    {'\n'}
                    ASEQ | Studentcare will return one transcript of the information
                    contained in our files. ASEQ | Studentcare may charge a reasonable
                    fee if further copies are requested.
                    {'\n'}
                    If, after reviewing the transcript of his or her file, a student
                    wishes to rectify any errors in the file, he or she may mail this
                    request, detailing the identified errors to:
                    {'\n'}
                    Attention: Information Correction
                    {'\n'}
                    ASEQ | Studentcare
                    {'\n'}
                    1200 McGill College Avenue, Suite 2200
                    {'\n'}
                    Montreal, QC  H3B 4G7
                </Text>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#faf2cc',
        padding: 20,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        color: '#333',
        textAlign: 'center',
        textTransform: 'uppercase',
        letterSpacing: 2,
    },
    text: {
        fontSize: 16,
        color: '#333',
        lineHeight: 24,
        textAlign: 'justify',
        marginBottom: 10,
    },
    logo: {
        width: 500,
        height: 300,
        resizeMode: 'contain',
        marginBottom: 20,
    },
    scrollView: {
        width: '100%',
    },
});
