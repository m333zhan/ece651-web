import { StyleSheet } from "react-native-web";
import { View, TouchableOpacity, Text } from 'react-native';

export default function Home({ navigation, route }) {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Home</Text>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('FileClaim')}>
                <Text style={styles.buttonText}>File A Claim</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Coverage')}>
                <Text style={styles.buttonText}>Coverage</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('ClaimHistory')}>
                <Text style={styles.buttonText}>Claim History</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('PayDirectCard')}>
                <Text style={styles.buttonText}>Pay Direct Card</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TravelEmergency')}>
                <Text style={styles.buttonText}>Travel Emergency</Text>
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#faf2cc",
        padding: 20,
    },
    title: {
        fontSize: 24,
        fontWeight: "bold",
        marginBottom: 20,
        color: "#333",
        textAlign: "center",
        textTransform: "uppercase",
        letterSpacing: 2,
    },
    button: {
        backgroundColor: "#007aff",
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 5,
        alignItems: "center",
        marginBottom: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#fff",
    },
});
