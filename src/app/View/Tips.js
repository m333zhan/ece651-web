import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';

export default function Tips({ navigation, route }) {
    const [showAnswer1, setShowAnswer1] = useState(false);
    const [showAnswer2, setShowAnswer2] = useState(false);
    const [showAnswer3, setShowAnswer3] = useState(false);
    const [showAnswer4, setShowAnswer4] = useState(false);
    const [showAnswer5, setShowAnswer5] = useState(false);
    const [showAnswer6, setShowAnswer6] = useState(false);
    const [showAnswer7, setShowAnswer7] = useState(false);

    return (
        <View style={styles.container}>
            <ScrollView>
            <Text style={styles.title}>Tips</Text>
            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer1(!showAnswer1)}>
                    <Text style={styles.question}>Making a Claim</Text>
                </TouchableOpacity>
                {showAnswer1 && (
                    <Text style={styles.answer}>
                        You first need to complete your claims profile. Begin byselecting the File a Claim button under the home option. You will then be asked who the claim is for, the type of claim (health, dental, vision), to provide or take pictures of your receipt, etc. Remember to keep your receipts for one full year. More information on making a claim can be found under the FAOmenu.
                        To access this feature, you'll first need to provide your address and add any dependants who are currently enrolled under your Plan. Please note that you cannot use this app to enroll family members. You can only enroll eligible dependants during the Change-of-Coverage Period at the beginning of the school year. Visit studentcare.ca or use.for deadlines and more information.
                        Your first time making a claim, you can select File a Claim under the Home menu. This will direct you to a form requesting the necessary information that will make claiming quick and easy in the future. Then select Home, File a Claim, and complete the steps on the screen. The app allows you to take pictures of your receipt.
                        To modify your profile information at a later date, you can select Profile from the menu at the bottom and fill out the information requested under Claim Preferences (contact information, dependants, and coordination of benefits).
                    </Text>
                )}
            </View>

            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer2(!showAnswer2)}>
                    <Text style={styles.question}>Adding dependents</Text>
                </TouchableOpacity>
                {showAnswer2 && (
                    <Text style={styles.answer}>
                        First, make sure that your dependants are enrolled in the PlanYou can only enroll eligible dependants during the Change-ofCoverage Period at the beginning of the school year. Visitstudentcare.ca or aseq.ca for deadlines and more information.
                        To claim for your dependants, you will need to complete your claim profile by selecting File a Claim under the Home menu. During this process, you will be asked about dependents. To update your dependant's information at a later date, select Profile and then Dependants under the claim preferences section.
                    </Text>
                )}
            </View>

            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer3(!showAnswer3)}>
                    <Text style={styles.question}>Adding aother insurers</Text>
                </TouchableOpacity>
                {showAnswer3 && (
                    <Text style={styles.answer}>
                        You will be asked about coverage under another insurer when you create a full profile for claiming. The information you provide can always be modified at a later date by selecting Profile from the menu at the bottom and then Coordination of benefits under the Claim Preferences menu.
                    </Text>
                )}
            </View>


            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer4(!showAnswer4)}>
                    <Text style={styles.question}>What to do in case of a travel emergency?</Text>
                </TouchableOpacity>
                {showAnswer4 && (
                    <Text style={styles.answer}>
                        if faced with a medical emergency while traveling outsideyour the province of residence or in a foreign country, please refer to the travel benefit section under the home menu. This option will appear only if you are covered under the Plan.
                    </Text>
                )}
            </View>

            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer5(!showAnswer5)}>
                    <Text style={styles.question}>What information to provide when emailing the MemberServices Centre</Text>
                </TouchableOpacity>
                {showAnswer5 && (
                    <Text style={styles.answer}>
                        Full name, student ID, school, question and any helpful background information. Include the claim number if applicable.
                    </Text>
                )}
            </View>


            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer1(!showAnswer1)}>
                    <Text style={styles.question}>What's the best way to claim prescription drugs?</Text>
                </TouchableOpacity>
                {showAnswer1 && (
                    <Text style={styles.answer}>
                        If your plan covers prescription drugs, there are two ways to claim.
                        1. If you have internet access, bring up the Pay Direct Carc(under Home and then Pay Direct Card) and show it to the pharmacist along with a valid student ID. The pharmacist will be able to process the claim immediately, so you won't have to pay the full amount upfront, submit a claim, or wait to be reimbursed. let's recommend that you take a screenshot (by pressing the home and power buttons at the same time on your iPod, iPhone.or iPad)in case you can't access the internet at the pharmacy.
                        2. If you don't have internet access or a screenshot of your card, you may pay the pharmacist up front and submit your claim through the regular claims process (see tip on making a claim above).
                    </Text>
                )}
            </View>

            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#faf2cc',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    questionBox: {
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        alignSelf: 'stretch',
        marginBottom: 20,
    },
    question: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#333',
        textAlign: 'justify',
    },
    answer: {
        fontSize: 16,
        color: '#333',
        lineHeight: 24,
        textAlign: 'justify',
        marginTop: 10,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        color: '#333',
        textAlign: 'center',
        textTransform: 'uppercase',
        letterSpacing: 2,
    },
});
