import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView} from 'react-native';

export default function FAQ({ navigation, route }) {
    const [showAnswer1, setShowAnswer1] = useState(false);
    const [showAnswer2, setShowAnswer2] = useState(false);
    const [showAnswer3, setShowAnswer3] = useState(false);
    const [showAnswer4, setShowAnswer4] = useState(false);
    const [showAnswer5, setShowAnswer5] = useState(false);
    const [showAnswer6, setShowAnswer6] = useState(false);
    const [showAnswer7, setShowAnswer7] = useState(false);
    return (
        <View style={styles.container}>
            <ScrollView>
            <Text style={styles.title}>Frequently Asked Questions</Text>
            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer1(!showAnswer1)}>
                    <Text style={styles.question}>Question 1: Why isn't the benefits section more detailed?</Text>
                </TouchableOpacity>
                {showAnswer1 && (
                    <Text style={styles.answer}>
                        Answer 1: The main purpose of the app is to allow you to quickly and securely make a claim. A summary is provided to help you get a basic idea of how much you will be reimbursed. Full details are always provided on your Plan website at studentcare.ca or aseq.ca
                    </Text>
                )}
            </View>




            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer2(!showAnswer2)}>
                    <Text style={styles.question}>Question 2: Why does the app ask for my banking information?</Text>
                </TouchableOpacity>
                {showAnswer2 && (
                    <Text style={styles.answer}>
                        Answer 2: Your information is always kept secure. This information is necessary for the insurer to provide our refund by direct deposit. Only direct deposit refunds are offered through the app. </Text>
                )}
            </View>


            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer3(!showAnswer3)}>
                    <Text style={styles.question}>Question 3: How secure is the app??</Text>
                </TouchableOpacity>
                {showAnswer3 && (
                    <Text style={styles.answer}>
                        Answer 3: Just as with our other online tools, we've also built the mobile app with security in mind. Here are the main security measures:
                        Account passwords are stored in encrypted form. All communication between the App and our servers is
                        encrypted using 128-bit encryption technology, the same as banks use on their websites.
                        Once the app successfully transmits the claim to our
                        servers, the information is removed from the app.
                        Claim information is securely transmitted to the insurer
                        using strong encryption.
                    </Text>
                )}
            </View>


            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer4(!showAnswer4)}>
                    <Text style={styles.question}>Question 4: Can I change my coverage through the app?</Text>
                </TouchableOpacity>
                {showAnswer4 && (
                    <Text style={styles.answer}>
                        Answer 4:No, at this time you cannot change your coverage through the app. if the option is available, you can visit studentcare.ca oraseq.ca during the Change-of-Coverage Period to opt out, change your benefits, and/or add dependants.
                    </Text>
                )}
            </View>


            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer5(!showAnswer5)}>
                    <Text style={styles.question}>Question 5: What happens if l change associations or schools fromone year to the next?</Text>
                </TouchableOpacity>
                {showAnswer5 && (
                    <Text style={styles.answer}>
                        Answer 5: Your claim history will remain stored in the app unless you choose to delete it. If you're covered by an undergraduate plan one year and the next year you change schools or arecovered under the graduate Plan, you will need to change your school and student ID but your claims history will remain on your profile.
                    </Text>
                )}
            </View>


            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer6(!showAnswer6)}>
                    <Text style={styles.question}>Question 6: How do I reset my password?</Text>
                </TouchableOpacity>
                {showAnswer6 && (
                    <Text style={styles.answer}>
                        Answer 6:if you are logged in, select Profile from the menu at the bottom and then click on the security settings option under the App Profile section. Once you have updated your password and chosen the automatic logout setting, select Submit to save your changes.
                        If you are unable to log in, scroll down the Get Started page and select Reset my password. You will be asked to enter your email, after which an email will be sent with directions on how to securely reset your password
                    </Text>
                )}
            </View>


            <View style={styles.questionBox}>
                <TouchableOpacity onPress={() => setShowAnswer7(!showAnswer7)}>
                    <Text style={styles.question}>Question 7: How do l update my address and phone number?</Text>
                </TouchableOpacity>
                {showAnswer7 && (
                    <Text style={styles.answer}>
                        Answer 7: Once you are logged in, select Profile from the menu at the bottom. Then select Contact Information under the ClaimPreferences section. Make sure to save once you have updated your information.
                    </Text>
                )}
            </View>

            </ScrollView>

          
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#faf2cc',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    questionBox: {
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        alignSelf: 'stretch',
        marginBottom: 20,
    },
    question: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#333',
        textAlign: 'justify',
    },
    answer: {
        fontSize: 16,
        color: '#333',
        lineHeight: 24,
        textAlign: 'justify',
        marginTop: 10,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        color: '#333',
        textAlign: 'center',
        textTransform: 'uppercase',
        letterSpacing: 2,
    },
});
