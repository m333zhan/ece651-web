import { View, Text, StyleSheet } from "react-native";

export default function TravelEmergency({ navigation, route }) {
    return (
        <View style={styles.container}>
            <View style={styles.section}>
                <Text style={styles.title}>Travel Emergency</Text>
                <Text style={styles.subtitle}>
                    Your travel emergency insurer:{" "}
                    <Text style={styles.textBold}>SunLife</Text>
                </Text>
                <Text style={styles.subtitle}>
                    Policy number: <Text style={styles.textBold}>4008-823-823</Text>
                </Text>
                <Text style={styles.info}>
                    To receive assistance for a medical emergency or travel assistance
                    during your travels contact Allianz Global Assistance:
                </Text>
                <Text style={styles.info}>
                    Toll-free (Canada & United States):{" "}
                    <Text style={styles.textBold}>1-800-511-4610</Text>
                </Text>
                <Text style={styles.info}>
                    Collect: <Text style={styles.textBold}>0-202-296-7493</Text>
                </Text>
                <Text style={styles.info}>
                    You will need to provide them with your student ID number and your
                    group policy number.
                </Text>
                <Text style={styles.info}>
                    <Text style={styles.textBold}>Note:</Text> Please contact Allianz
                    Global Assistance prior to seeking medical treatment so that they can
                    assess your situation, help you locate a suitable provider in your
                    area, and open a medical case to monitor your care. They will monitor
                    your care until treatment is complete.
                </Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: "#faf2cc",
    },
    section: {
        marginBottom: 20,
        backgroundColor: "#fff",
        padding: 20,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    title: {
        fontSize: 24,
        fontWeight: "bold",
        marginBottom: 20,
        color: "#333",
        textAlign: "center",
        textTransform: "uppercase",
        letterSpacing: 2,
    },
    subtitle: {
        fontSize: 20,
        fontWeight: "bold",
        marginTop: 10,
        marginBottom: 10,
        color: "#666",
        textTransform: "uppercase",
        letterSpacing: 1,
    },
    info: {
        fontSize: 16,
        marginBottom: 10,
        color: "#777",
        lineHeight: 24,
    },
    textBold: {
        fontWeight: "bold",
        color: "#333",
    },
});
