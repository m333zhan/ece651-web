import React, { useEffect, useContext, useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import Constants from 'expo-constants';
import { UserContext } from "../Context/UserContext";
import ErrorPopup from './ErrorPopup';

export default function ClaimHistory({ navigation, route }) {
    const [loading, setLoading] = useState(true);
    const [previousClaim, setPreviousClaim] = useState([]);
    const { user, updateUser } = useContext(UserContext);

    useEffect(() => {
        const loadPost = async () => {
            if (!loading) return;
            var request = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Authorization': 'Bearer ' + user.userToken
                },
                body: JSON.stringify({ user_name: user.username })
            }
            const response = await fetch(`http://${Constants.expoConfig.extra.apiUrl}:${Constants.expoConfig.extra.apiPort}/claimHistory`, request);
            if(!response.ok){
                response.json().then(data => {
                    ErrorPopup("Error", data.error,
                        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
                    )
                })
            }
            else {
                const data = await response.json();
                setPreviousClaim(data["claimHistory"]);
                setLoading(false);
            }
        }
        loadPost();
    });

    return (
        <>
            {
                loading ?
                    (<Text style={styles.loading}>Loading...</Text>) :
                    (<View style={styles.container}>
                        <View style={styles.table}>
                            {
                                (previousClaim.map((claim) =>
                                    <View style={[styles.row, styles.headerRow]}>
                                        <Text style={[styles.cell, styles.headerCell]}>{claim["id"]}</Text>
                                        <Text style={[styles.cell, styles.headerCell]}>{claim["date"]}</Text>
                                        <Text style={[styles.cell, styles.headerCell]}>{claim["type"]}</Text>
                                        <Text style={[styles.cell, styles.headerCell]}>{claim["amount"]}</Text>
                                        <Text style={[styles.cell, styles.headerCell]}>{claim["status"]}</Text>
                                        <Text style={[styles.cell, styles.headerCell]}>{claim["add-on"]}</Text>
                                    </View>
                                ))
                            }
                        </View>
                    </View>)
            }
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#faf2cc',
    },
    loading: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#333',
        textAlign: 'center',
        paddingTop: 50,
    },
    table: {
        borderWidth: 1,
        borderColor: "#333",
        marginBottom: 20,
    },
    row: {
        flexDirection: "row",
    },
    headerRow: {
        backgroundColor: "#333",
    },
    cell: {
        flex: 1,
        padding: 10,
        borderWidth: 1,
        borderColor: "#333",
        color: '#333',
        textAlign: 'center',
    },
    headerCell: {
        fontWeight: "bold",
        color: '#fff',
        textAlign: 'center',
    },
});
