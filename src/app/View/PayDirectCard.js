import { View, Text, Image, StyleSheet } from 'react-native';

export default function PayDirectCard({ navigation, route }) {
    return (
        <View style={styles.container}>
            <View style={styles.card}>
                <Text style={styles.title}>PayDirect Card</Text>
                <Text style={styles.subtitle}>
                    Your PayDirect Card is your health insurance coverage card that you can use to pay
                    for eligible health-care expenses.
                </Text>
                <Text style={styles.info}>
                    You can use your PayDirect Card to pay for eligible expenses at pharmacies and other
                    health-care providers that accept UHIP.
                </Text>
                <Text style={styles.info}>
                    You can check your coverage under the coverage section.
                </Text>
                <Text style={styles.info}>
                    If you have any issues with your PayDirect Card or need to report a lost or stolen
                    card, please contact PayDirect customer service at{" "}
                    <Text style={styles.infoClickable}>4008-823-823</Text>.
                </Text>
                <Image
                    source={require('../../assets/paydirectcard.png')}
                    style={styles.image}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#faf2cc',
    },
    card: {
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        color: '#333',
        textAlign: 'center',
        textTransform: 'uppercase',
        letterSpacing: 2,
    },
    subtitle: {
        fontSize: 18,
        marginBottom: 20,
        color: '#777',
        lineHeight: 24,
    },
    info: {
        fontSize: 16,
        marginBottom: 5,
        color: '#777',
        lineHeight: 24,
    },
    infoClickable: {
        fontSize: 16,
        marginBottom: 5,
        color: '#007aff',
        textDecorationLine: 'underline',
    },
    image: {
        width: '100%',
        height: 400,
        resizeMode: 'contain',
        marginTop: 20,
    },
});
