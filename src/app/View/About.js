import React from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';

export default function About({ navigation, route }) {
    return (
        <View style={styles.container}>
            <View style={styles.aboutBox}>
                <Text style={styles.title}>About Us</Text>

                <ScrollView style={styles.scrollView}>
                    <Text style={styles.text}>
                        This mobile app is a service of ASEQ | Studentcare, the largest Canadian provider of Health & Dental Plans for students.{'\n'}
                        {'\n'}
                        Established in 1996 to serve the health and dental care needs of Canadian post-secondary students, ASEQ | Studentcare now serves nearly 750,000 members at over 40 post-secondary institutions and over 75 student associations.
                    </Text>
                </ScrollView>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#faf2cc',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    aboutBox: {
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        alignSelf: 'stretch',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        color: '#333',
        textAlign: 'center',
        textTransform: 'uppercase',
        letterSpacing: 2,
    },
    text: {
        fontSize: 16,
        color: '#333',
        lineHeight: 24,
        textAlign: 'justify',
        marginBottom: 10,
    },
    scrollView: {
        width: '100%',
    },
});
