import { useContext, useState } from "react";
import { View, Text, TextInput, ScrollView, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';
import { RadioButton } from 'react-native-paper';
import { UserContext } from "../Context/UserContext";
import ErrorPopup from './ErrorPopup';
import { StyleSheet } from 'react-native';

export default function FileClaim ({ navigation, route }) {
	const { user, updateUser } = useContext(UserContext);
	const [ typeClaim, setTypeClaim ] = useState("");
	const [ claimImage, setClaimImage ] = useState(null);

	let workInjury = false, 
		moterVehicleAccicident = false, 
		outOfCanada = false,
		resultOfAccicident = false,
		claimText = "";

	const handleWorkInjury = (event) => {
		workInjury = event.target.checked;
	}

	const handleMoterVehicleAccicident = (event) => {
		moterVehicleAccicident = event.target.checked;
	}

	const handleOutOfCanada = (event) => {
		outOfCanada = event.target.checked;
	}

	const handleResultOfAccicident = (event) => {
		resultOfAccicident = event.target.checked;
	}

	const handleClaimImageChange = e => {
		const selectedFile = e.target.files[0];
		const reader = new FileReader();
		reader.onload = event => {
			setClaimImage(event.target.result);
		};
		reader.readAsDataURL(selectedFile);
	}

	const handleUpload = async () => {
		const formData = new FormData();
		formData.append('image', claimImage);
		formData.append('comment', claimText);
		formData.append('user_name', user.username);
		formData.append('type', typeClaim);
		let claimAddon = "";
		if(typeClaim === "dental"){
			claimAddon = "Result of Accicident: " + resultOfAccicident.toString();
		}
		else{
			claimAddon = "Work Injery: " + workInjury.toString() + "\n";
			claimAddon += "Motor Vehicle Accicident" + moterVehicleAccicident.toString() + "\n";
			claimAddon += "Out of Canada: " + outOfCanada.toString() + "\n";
		}
		formData.append("add_on", claimAddon);
		try {
			const response = await fetch(`http://${Constants.expoConfig.extra.apiUrl}:${Constants.expoConfig.extra.apiPort}/claim`, {
				method: 'POST',
				headers: { 
					'Authorization': 'Bearer ' + user.userToken
				},
				body: formData
			});
			if(!response.ok){
				response.json().then(data => {
					ErrorPopup("Error", data.error,
					[{text: 'OK', onPress: () => console.log('OK Pressed')}],
					)
				})
			}
			else {
				navigation.navigate('Home');
			}
		} catch (error) {
		  console.error(error);
		}
	  };

	function generateButton(){
		if(typeClaim === "health" || typeClaim === "vision"){
			return(
				<View>
					<span>Is it a Work Injury ?</span>
					<input 
						type="checkbox"
						onChange={handleWorkInjury}
					/>
					<span>Is it a Moter Vehicle Accicident?</span>
					<input 
						type="checkbox"
						onChange={handleMoterVehicleAccicident}
					/>
					<span>Is it happened out of Canada ?</span>
					<input 
						type="checkbox"
						onChange={handleOutOfCanada}
					/>
				</View>
			)
		}
		else if(typeClaim === "dental"){
			return(
				<View>
					<span>Is it result of Accicident ?</span>
					<input 
						type="checkbox"
						onChange={handleResultOfAccicident}
					/>
				</View>
			)
		}
		else return(<View></View>)
	}

	return (
		<View style={styles.container}>
			<ScrollView>
				<Text style={styles.title}>Type of Claim</Text>
				<View style={styles.radioGroup}>
					<RadioButton.Group
						onValueChange={value => setTypeClaim(value)}
						value={typeClaim}>
						<RadioButton.Item label="Health" value="health" />
						<RadioButton.Item label="Vision" value="vision" />
						<RadioButton.Item label="Dental" value="dental" />
					</RadioButton.Group>
				</View>
				{generateButton()}
				<Text style={styles.subtitle}>Upload Claim image</Text>
				<View style={styles.fileInputContainer}>
					<input style={styles.fileInput} type="file" onChange={handleClaimImageChange} />
				</View>
				<TextInput
					style={styles.textInput}
					multiline={true}
					numberOfLines={4}
					onChangeText={(text) => claimText = text}
					placeholder="Add a comment about your claim"
				/>
				<TouchableOpacity style={styles.button} onPress={handleUpload}>
					<Text style={styles.buttonText}>Upload</Text>
				</TouchableOpacity>
			</ScrollView>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
		backgroundColor: "#faf2cc",
	},
	title: {
		fontSize: 24,
		fontWeight: "bold",
		marginBottom: 20,
		color: "#333",
		textAlign: "center",
		textTransform: "uppercase",
		letterSpacing: 2,
	},
	radioGroup: {
		marginBottom: 20,
	},
	subtitle: {
		fontSize: 20,
		fontWeight: "bold",
		color: "#666",
		textTransform: "uppercase",
		letterSpacing: 1,
		marginLeft: 10,
		marginBottom: 10,
	},
	textInput: {
		fontSize: 16,
		borderColor: "#777",
		borderWidth: 1,
		borderRadius: 5,
		marginBottom: 20,
		paddingHorizontal: 10,
		paddingTop: 10,
		paddingBottom: 10,
		backgroundColor: "#fff",
	},
	button: {
		backgroundColor: "#007aff",
		paddingHorizontal: 20,
		paddingVertical: 10,
		borderRadius: 5,
		alignItems: "center",
	},
	buttonText: {
		fontSize: 16,
		fontWeight: "bold",
		color: "#fff",
	},
	fileInputContainer: {
		marginBottom: 20,
	},
	fileInput: {
		marginBottom: 10,
	},
});