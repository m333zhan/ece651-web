﻿import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';

export default function TermCondition({ navigation, route }) {
    return (
        <View style={styles.container}>
            <Image source={require('../../assets/logo-student-care.png')} style={styles.logo} />
            <Text style={styles.title}>Terms & Conditions</Text>
            <ScrollView style={styles.scrollView}>
                <Text style={styles.text}>
                    TERMS OF USE{'\n'}
                    {'\n'}
                    All services and information made available to users through the use of this Application are covered by the terms and conditions described below. By registering for and using the Studentcare and/or ASEQ mobile app, you agree to abide by these terms and conditions.

                    We reserve the right to change the terms and conditions at any time, without advance notice. In such cases, ASEQ | Studentcare may post change notices directly in the Application or may provide links to such change notices.

                    The ASEQ and Studentcare mobile apps are free to download, however standard wireless carrier message and data rates may apply. Please consult your wireless carrier for applicable data plan rates.
                    {'\n'}
                    {'\n'}
                    JURISDICTION
                    {'\n'}
                    The Application is managed by ASEQ | Studentcare from its offices within the province of Quebec, Canada. By accessing, registering for, or using the Application you agree that all matters relating to your access to or use of the Application and/or its content shall be governed by and construed in accordance with the laws of the province of Quebec. All disputes, controversies, or claims arising out of or in connection with the site shall be submitted to and be subject to the jurisdiction of the courts of the province of Quebec. Thus, in the event of any suit, action, or proceeding arising out of or in connection with this Application, the courts of the province of Quebec will have exclusive jurisdiction. You agree and hereby submit to the non-exclusive jurisdiction of the Federal Court of Canada and/or the courts of the province of Quebec with respect to such matters.

                    {'\n'}
                    {'\n'}
                    REGISTRATION
                    {'\n'}
                    To use certain features of the Application, you may be asked to register with the Application. You agree (i) to provide true, accurate, current, and complete information about yourself as prompted by any registration form; and (ii) to maintain and promptly update the information you provide to keep it true, accurate, current, and complete. If ASEQ | Studentcare has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, ASEQ | Studentcare has the right to suspend or terminate your access to and use of the Application (or any portion thereof). ASEQ | Studentcare’s use of any personally identifying information you provide as part of the registration process is governed by the terms of our Privacy Policy.

                    {'\n'}
                    {'\n'}
                    LIMITATION OF LIABILITY
                    {'\n'}
                    To the fullest extent possible pursuant to applicable law, ASEQ | Studentcare will in no way be liable for any direct or indirect damage, including without limitation the loss of data, even if ASEQ | STUDENTCARE has been advised about the possibility of such damage, that may arise:

                    ‐ from the use or inability to use the Application
                    ‐ following unauthorized access to the Application by a user or following a change in your information
                    ‐ from the actions or conduct of a third party in the course of using the Application

                    ASEQ | Studentcare reserves the right at any time to change, temporarily suspend, or terminate the Application in whole or in part without informing you in advance. In no event will ASEQ | Studentcare be liable to you or to any third party for a modification, suspension, or termination of the Application.

                    {'\n'}
                    {'\n'}
                    OWNERSHIP
                    {'\n'}
                    All materials displayed or otherwise accessible through the Application, including, without limitation, text, images, computer software, and code (collectively, the Content) are protected under Canadian and foreign copyright or other laws, and are owned by ASEQ | Studentcare, its licensors or the party accredited as the provider of the Content. In addition, the Application is protected under copyright law as a collective work and/or compilation pursuant to Canadian and foreign laws. You shall abide by all additional copyright notices, information, and restrictions on or contained in any of the Content accessed through the Application. ANY USE, REPRODUCTION, ALTERATION, MODIFICATION, UPLOADING OR POSTING ONTO THE INTERNET, TRANSMISSION, REDISTRIBUTION OR OTHER EXPLOITATION OF THE APPLICATION OR OF ANY CONTENT, WHETHER IN WHOLE OR IN PART, OTHER THAN EXPRESSLY SET OUT HEREIN, IS PROHIBITED WITHOUT THE EXPRESS WRITTEN PERMISSION OF ASEQ | STUDENTCARE.


                    {'\n'}
                    {'\n'}
                    RESTRICTION ON USE OF CONTENT
                    {'\n'}It is prohibited to reproduce by any means whatsoever the information, data, text, software, music, sounds, photos, images, videos, communications, and other materials (collectively the Content) included with the Application.

                    More specifically, it is prohibited to:

                    ‐ forge headers or otherwise manipulate content transmitted through the Application• copy, alter, rent, borrow, sell, distribute, or create derivative works based in whole or in part on the Content, the Application or related Software and Applications
                    ‐ upload, post, email, transmit or otherwise make available any content to the Application that contains computer viruses or other computer code, files or programs designed to interrupt, destroy or limit the functionality of any software, computer, application, or telecommunications equipment (this list may include other elements without limitation)
                    ‐ interfere with, disrupt, or make abusive use of the Application, or servers or networks connected to the Application, or refuse to comply with any requirements, procedures, general regulations, or regulatory provisions applicable to networks connected to the Application
                    ‐ infringe, whether intentionally or not, any existing national or international laws or regulations collect and store personal information relating to other users
                    ‐ reverse engineer, disassemble or otherwise attempt to locate the source code (except as provided for by law) of the Application
                    ‐ sell, resell, or operate on any commercial basis whatsoever any portion of the Application or any use of the Application or access privileges associated with the Application
                    ‐ access the Application by any means other than the interface provided to you by the Application for this purpose

                    All information in this Application is protected by copyright. Users may only copy its contents for their own personal use. No one may redistribute, reproduce, republish, store in any medium, retransmit, or modify the information in this Application for commercial purposes without the written authorization of ASEQ | Studentcare, or make public use thereof without crediting the source.

                    All trademarks and logos (including certain names, words, titles, drawings, illustrations and icons) appearing in the Application are trademarks or registered trademarks of ASEQ | Studentcare or third parties. Any use of these trademarks is strictly forbidden without the owner's written authorization. Any unauthorized downloading, retransmission, or other copying or modification of trademarks and/or Application content may be a violation of federal or other laws applying to trademarks and/or copyrights and could subject the copier to legal action.
                    {'\n'}
                    {'\n'}
                    © 2018 ASEQ | Studentcare. All rights reserved. This application is protected under the copyright laws of Canada and other countries. In addition, certain information may be copyrighted by others. Unless otherwise specified, no one has permission to copy, retransmit, redistribute, reproduce or republish any part or aspect of this Application in any form, or any information found in this application, whatsoever. Persons interested in securing permission should contact ASEQ | Studentcare.
                </Text>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#faf2cc',
        padding: 20,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        color: '#333',
        textAlign: 'center',
        textTransform: 'uppercase',
        letterSpacing: 2,
    },
    text: {
        fontSize: 16,
        color: '#333',
        lineHeight: 24,
        textAlign: 'justify',
        marginBottom: 10,
    },
    logo: {
        width: 500,
        height: 300,
        resizeMode: 'contain',
        marginBottom: 20,
    },
    scrollView: {
        width: '100%',
    },
});
