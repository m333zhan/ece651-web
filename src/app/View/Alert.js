import { View, Text, Image, StyleSheet } from "react-native";

export default function Alert() {
    return (
        <View style={styles.container}>
            <View style={styles.alertBox}>
                <Image
                    style={styles.image}
                    source={{
                        uri: "https://th.bing.com/th/id/R.68879c7054808c52c9ec77e3458eaf31?rik=XfsROF%2fN%2fTXQTQ&pid=ImgRaw&r=0",
                    }}
                />
                <Text style={styles.alertTitle}>ALERTS</Text>
                <Text style={styles.alertDate}>2019-03-29</Text>
                <Text style={styles.alertMessage}>
                    As of April 1, ON residents 24 and under with access to a private plan
                    covering prescription drugs, including student plans, will no longer be
                    eligible for OHIP+. Prescription drug claims incurred on or after April
                    1 must be submitted directly through the student Health Plan.
                </Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: "#faf2cc",
        justifyContent: "flex-start",
        alignItems: "flex-start",
    },
    alertBox: {
        backgroundColor: "#fff",
        padding: 20,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        alignSelf: "stretch",
    },
    image: {
        width: 200,
        height: 200,
        alignSelf: "center",
    },
    alertTitle: {
        fontSize: 24,
        fontWeight: "bold",
        marginTop: 20,
        color: "#333",
        textAlign: "center",
        textTransform: "uppercase",
        letterSpacing: 2,
    },
    alertDate: {
        fontSize: 16,
        fontWeight: "bold",
        marginTop: 10,
        color: "#666",
        textAlign: "center",
    },
    alertMessage: {
        fontSize: 16,
        marginTop: 10,
        color: "#777",
        textAlign: "justify",
    },
});
