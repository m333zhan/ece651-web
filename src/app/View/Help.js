import { View, Text, StyleSheet } from 'react-native';

export default function Help({ navigation, route }) {
    const faqs = [
        {
            question: "How do I reset my password?",
            answer: "To reset your password, go to the login screen and click on the 'Forgot Password' link. Enter your email address and follow the instructions sent to your email.",
        },
        {
            question: "How can I update my personal information?",
            answer: "You can update your personal information by going to the 'Profile' section in the app. There, you can edit your information and save the changes.",
        },
        {
            question: "How do I file a claim?",
            answer: "To file a claim, navigate to the 'File A Claim' section in the app. Fill out the required information, upload any necessary documents, and submit the claim.",
        },
        {
            question: "How can I check the status of my claim?",
            answer: "You can check the status of your claim by visiting the 'Claim History' section in the app. This will display a list of all your submitted claims and their current status.",
        },
    ];

    return (
        <View style={styles.container}>
            <View style={styles.section}>
                <Text style={styles.title}>MEMBER SERVICES CENTRE</Text>
                <Text style={styles.info}>
                    You can reach the Member Services Centre from Mon. - Fri. from 9 am to 5 pm.
                </Text>
                <Text style={styles.info}>Phone: 519-904-2600</Text>
                <Text style={styles.info}>Email: mobi.service@studentcare.ca</Text>
            </View>
            <View style={styles.section}>
                <Text style={styles.title}>FREQUENTLY ASKED QUESTIONS</Text>
                {faqs.map((faq, index) => (
                    <View key={index} style={styles.faqItem}>
                        <Text style={styles.faqQuestion}>{faq.question}</Text>
                        <Text style={styles.faqAnswer}>{faq.answer}</Text>
                    </View>
                ))}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: "#faf2cc",
    },
    section: {
        marginBottom: 20,
        backgroundColor: "#fff",
        padding: 20,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 20,
        color: "#333",
        textAlign: "center",
        textTransform: "uppercase",
        letterSpacing: 2,
    },
    info: {
        fontSize: 16,
        marginBottom: 10,
        color: "#777",
    },
    faqItem: {
        marginBottom: 15,
    },
    faqQuestion: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#444",
    },
    faqAnswer: {
        fontSize: 16,
        color: "#666",
        paddingLeft: 10,
    },
});
