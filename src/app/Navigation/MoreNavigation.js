import { createStackNavigator } from "@react-navigation/stack";
import More from '../View/More'
import About from '../View/About';
import PrivacySecurity from '../View/PrivacySecurity';
import Tips from '../View/Tips';
import FAQ from '../View/FAQ';
import TermCondition from '../View/TermCondition';

const Stack = createStackNavigator();
export const MoreNavigator = () => {
  return (
		<Stack.Navigator 
			initialRouteName="More"
			screenOptions={{
                headerShown: false
            }}
        >
			<Stack.Screen 
				name="More" 
				component={More}
				options={{
					headerBackTitleVisible: false
				}}
			/>
			<Stack.Screen 
				name="About" 
				component={About} 
				/>
			<Stack.Screen 
				name="FAQ" 
				component={FAQ} 
				/>
			<Stack.Screen 
				name="PrivacySecurity" 
				component={PrivacySecurity} 
				/>
			<Stack.Screen 
				name="TermCondition" 
				component={TermCondition} 
				/>
            <Stack.Screen 
				name="Tips" 
				component={Tips} 
				/>
		</Stack.Navigator>
  );
}
