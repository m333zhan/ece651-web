import { createStackNavigator } from "@react-navigation/stack";
import FileClaim from "../View/FileClaim";
import Coverage from "../View/Coverage";
import PayDirectCard from "../View/PayDirectCard";
import ClaimHistory from "../View/ClaimHistory";
import TravelEmergency from "../View/TravelEmergency";
import Home from "../View/Home";


const Stack = createStackNavigator();

export const MainNavigator = () => {
  	return (
		<Stack.Navigator 
			initialRouteName="Home"
			screenOptions={{
        	headerShown: false
		}}
		>
			<Stack.Screen 
				name="Home" 
				component={Home}
				options={{
					headerBackTitleVisible: false
				}}
			/>
			<Stack.Screen 
				name="FileClaim" 
				component={FileClaim} 
		  />
		  <Stack.Screen
			  name="ClaimHistory"
			  component={ClaimHistory}
		  />
			<Stack.Screen 
				name="Coverage" 
				component={Coverage} 
				/>
			<Stack.Screen 
				name="PayDirectCard" 
				component={PayDirectCard} 
				/>
			<Stack.Screen 
				name="TravelEmergency" 
				component={TravelEmergency} 
				/>
		</Stack.Navigator>
  );
}
