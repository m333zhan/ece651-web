import 'react-native-gesture-handler';
import { useContext } from 'react';
import { Button } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Constants from 'expo-constants';

import { UserContext } from '../Context/UserContext';
import { MainNavigator } from './MainNavigation';
import { MoreNavigator } from './MoreNavigation';
import Alert from '../View/Alert';
import Help from '../View/Help';
import Profile from '../View/Profile';

const Tab = createBottomTabNavigator();

export const FooterNavigator = ({ navigation, route }) => {
  const { user, updateUser } = useContext(UserContext);
  function logout() {
    var request = {
      method: 'POST',
      headers: { 
        'Authorization': 'Bearer ' + user.userToken
      },
    }
    fetch(`http://${Constants.expoConfig.extra.apiUrl}:${Constants.expoConfig.extra.apiPort}/auth/logout`,request)
    .then(response => {
      if(!response.ok){
        response.json().then(data => {
          ErrorPopup("Error", data.error,
          [{text: 'OK', onPress: () => console.log('OK Pressed')}],
          )
        })
      }
      else {
        navigation.navigate('Login')
      }
    });
  }
  return (
    <Tab.Navigator
      initialRouteName="Main"
      shifting={true}
      sceneAnimationEnabled={false}
      screenOptions={{
        headerShown: true
      }}
      >
      <Tab.Screen
        name="Main"
        component={MainNavigator}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
          headerRight: () => (
            <Button
              onPress={logout}
              title="Logout"
              color="#000"
            />
          ),
        }}
      />
      <Tab.Screen
        name="Alert"
        component={Alert}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="alert" color={color} size={size} />
          ),
          headerRight: () => (
            <Button
              onPress={logout}
              title="Logout"
              color="#000"
            />
          ),
        }}
      />
      <Tab.Screen
        name="Help"
        component={Help}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="help" color={color} size={size} />
          ),
          headerRight: () => (
            <Button
              onPress={logout}
              title="Logout"
              color="#000"
            />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account-outline" color={color} size={size} />
          ),
          headerRight: () => (
            <Button
              onPress={logout}
              title="Logout"
              color="#000"
            />
          ),
        }}
      />
      <Tab.Screen
        name="More"
        component={MoreNavigator}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="more" color={color} size={size} />
          ),
          headerRight: () => (
            <Button
              onPress={logout}
              title="Logout"
              color="#000"
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}