import { createStackNavigator } from '@react-navigation/stack';
import LoginPage from "../View/Login";
import Register from '../View/Register';
import { FooterNavigator } from './FooterNavigation';

const Stack = createStackNavigator();

export default function AuthNavigator() {
  return (
    <Stack.Navigator 
      initialRouteName="Login"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen 
        name="Login" 
        component={LoginPage}
      />
      <Stack.Screen 
        name="Register" 
        component={Register}
      />
      <Stack.Screen 
        name="Tab" 
        component={FooterNavigator}
      />
    </Stack.Navigator>
  );
}
