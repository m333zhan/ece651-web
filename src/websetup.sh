#!/bin/bash

cd webSrc/src

echo "module.exports = {extra: {apiUrl: '$WEB_SERVER_CONNECTION_HOST', apiPort: $WEB_SERVER_CONNECTION_CONNECTION_PORT}};" > app.config.js

service ssh start

npm i

npx expo start --web