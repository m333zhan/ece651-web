import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AuthNavigator from './app/Navigation/AuthNavigation';
import { UserContext } from './app/Context/UserContext';

export default function App() {
  const [user, setUser] = React.useState({
    username: "",
    userToken: "",
  });
  const updateUser = newData => {
    setUser(prevData => ({
      ...prevData,
      ...newData,
    }));
  };

  return (
    <UserContext.Provider value={{user, updateUser}}>
      <NavigationContainer>
        <AuthNavigator />
      </NavigationContainer>
    </UserContext.Provider>
  );
}