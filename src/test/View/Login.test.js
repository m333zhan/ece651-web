import React from 'react';
import renderer from 'react-test-renderer';
import Login from '../../app/View/Login';
import { UserContext } from '../../app/Context/UserContext';
describe('<Login />', () => {
  it('has 4 child', () => {
    const user = { name: 'John Doe', email: 'johndoe@example.com' };
    const updateUser = jest.fn();
    const tree = renderer.create(
        <UserContext.Provider value={{ user, updateUser }}>
          <Login />
        </UserContext.Provider>
        ).toJSON();
    expect(tree.children.length).toBe(6);
  });
});

