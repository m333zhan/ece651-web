import React from 'react';
import renderer from 'react-test-renderer';
import Home from '../../app/View/Home';
import { UserContext } from '../../app/Context/UserContext';
describe('<Home />', () => {
  it('has 6 child', () => {
    const user = { name: 'John Doe', email: 'johndoe@example.com' };
    const updateUser = jest.fn();
    const tree = renderer.create(
        <UserContext.Provider value={{ user, updateUser }}>
          <Home />
        </UserContext.Provider>
        ).toJSON();
    expect(tree.children.length).toBe(6);
  });
});Home

