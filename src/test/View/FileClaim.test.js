import React from 'react';
import renderer from 'react-test-renderer';
import FileClaim from '../../app/View/FileClaim';
import { UserContext } from '../../app/Context/UserContext';
describe('<FileClaim />', () => {
  it('has 1 child', () => {
    jest.useFakeTimers()
    const user = { name: 'John Doe', email: 'johndoe@example.com' };
    const updateUser = jest.fn();
    const tree = renderer.create(
        <UserContext.Provider value={{ user, updateUser }}>
          <FileClaim />
        </UserContext.Provider>
        ).toJSON();
    expect(tree.children.length).toBe(1);
  });
});

