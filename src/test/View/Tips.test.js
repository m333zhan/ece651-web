import React from 'react';
import renderer from 'react-test-renderer';
import Tips from '../../app/View/Tips';
describe('<Tips />', () => {
  it('has 1 child and text is TermConidtion', () => {
    const tree = renderer.create(<Tips />).toJSON();
    expect(tree.children.length).toBe(1);
    expect(tree.children[0]
      .children[0].children[0].children[0]).toBe("Tips");
  });
});
