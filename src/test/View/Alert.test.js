import React from 'react';
import renderer from 'react-test-renderer';
import Alert from '../../app/View/Alert';
describe('<Alert />', () => {
  it('has 1 child and contains a date', () => {
    const tree = renderer.create(<Alert />).toJSON();
    expect(tree.children.length).toBe(1);
    expect(tree.children[0].children[2].children[0]).toBe("2019-03-29");
  });
});
