import React from 'react';
import renderer from 'react-test-renderer';
import Profile from '../../app/View/Profile';
import { UserContext } from '../../app/Context/UserContext';
describe('<Profile />', () => {
  it('has 1 child', () => {
    const user = { name: 'John Doe', email: 'johndoe@example.com' };
    const updateUser = jest.fn();
    const tree = renderer.create(
        <UserContext.Provider value={{ user, updateUser }}>
          <Profile />
        </UserContext.Provider>
        ).toJSON();
    expect(tree.children.length).toBe(1);
  });
});

