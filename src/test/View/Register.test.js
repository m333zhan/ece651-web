import React from 'react';
import renderer from 'react-test-renderer';
import Register from '../../app/View/Register';
import { UserContext } from '../../app/Context/UserContext';
describe('<Register />', () => {
  it('has 4 child', () => {
    const user = { name: 'John Doe', email: 'johndoe@example.com' };
    const updateUser = jest.fn();
    const tree = renderer.create(
        <UserContext.Provider value={{ user, updateUser }}>
          <Register />
        </UserContext.Provider>
        ).toJSON();
    expect(tree.children.length).toBe(7);
  });
});

