import React from 'react';
import renderer from 'react-test-renderer';
import About from '../../app/View/About';
describe('<About />', () => {
  it('has 1 child and text is About', () => {
    const tree = renderer.create(<About />).toJSON();
    expect(tree.children.length).toBe(1);
    expect(tree.children[0].children[0].children[0]).toBe("About Us");
  });
});
