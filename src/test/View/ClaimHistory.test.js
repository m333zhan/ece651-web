import React from 'react';
import renderer from 'react-test-renderer';
import ClaimHistory from '../../app/View/ClaimHistory';
import { UserContext } from '../../app/Context/UserContext';
describe('<ClaimHistory />', () => {
  it('has 1 child', () => {
    const user = { name: 'John Doe', email: 'johndoe@example.com' };
    const updateUser = jest.fn();
    const tree = renderer.create(
        <UserContext.Provider value={{ user, updateUser }}>
          <ClaimHistory />
        </UserContext.Provider>
        ).toJSON();
    expect(tree.children.length).toBe(1);
  });
});

