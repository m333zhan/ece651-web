import React from 'react';
import renderer from 'react-test-renderer';
import FAQ from '../../app/View/FAQ'
describe('<FAQ />', () => {
  it('has 1 child and text is FAQ', () => {
    const tree = renderer.create(<FAQ />).toJSON();
    expect(tree.children.length).toBe(1);
    expect(tree.children[0]
      .children[0].children[0].children[0]).toBe("Frequently Asked Questions");
  });
});
