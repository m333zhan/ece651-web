import React from 'react';
import renderer from 'react-test-renderer';
import PrivacySecurity from '../../app/View/PrivacySecurity'
describe('<PrivacySecurity />', () => {
  it('has 1 child and text is PrivacySecurity', () => {
    const tree = renderer.create(<PrivacySecurity />).toJSON();
    expect(tree.children.length).toBe(3);
    expect(tree.children[1].children[0]).toBe("Privacy & Security");
  });
});
