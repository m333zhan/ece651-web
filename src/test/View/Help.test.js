import React from 'react';
import renderer from 'react-test-renderer';
import Help from '../../app/View/Help'
describe('<Help />', () => {
  it('has 1 child and contains a header', () => {
    const tree = renderer.create(<Help />).toJSON();
    expect(tree.children.length).toBe(2);
    expect(tree.children[0]
        .children[0].children[0]).toBe("MEMBER SERVICES CENTRE");
  });
});
