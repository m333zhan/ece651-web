import React from 'react';
import renderer from 'react-test-renderer';
import PayDirectCard from '../../app/View/PayDirectCard'
describe('<PayDirectCard />', () => {
  it('has 1 child and text is PayDirectCard', () => {
    const tree = renderer.create(<PayDirectCard />).toJSON();
    expect(tree.children.length).toBe(1);
    expect(tree.children[0].children[0].children[0]).toBe("PayDirect Card");
  });
});
