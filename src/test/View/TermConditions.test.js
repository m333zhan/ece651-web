import React from 'react';
import renderer from 'react-test-renderer';
import TermCondition from '../../app/View/TermCondition';
describe('<TermCondition />', () => {
  it('has 1 child and text is TermConidtion', () => {
    const tree = renderer.create(<TermCondition />).toJSON();
    expect(tree.children.length).toBe(3);
    expect(tree.children[1].children[0]).toBe("Terms & Conditions");
  });
});
