import React from 'react';
import renderer from 'react-test-renderer';
import Coverage from '../../app/View/Coverage'
describe('<Coverage />', () => {
  it('has 1 child and contains a header', () => {
    const tree = renderer.create(<Coverage />).toJSON();
    expect(tree.children.length).toBe(1);
    expect(tree.children[0]
        .children[0].children[0]).toBe("YOUR PLAN BENEFITS");
  });
});
