import React from 'react';
import renderer from 'react-test-renderer';
import TipsTravelEmergency from '../../app/View/TravelEmergency'
describe('<TipsTravelEmergency />', () => {
  it('has 1 child and contains a header', () => {
    const tree = renderer.create(<TipsTravelEmergency />).toJSON();
    expect(tree.children.length).toBe(1);
    expect(tree.children[0]
        .children[0].children[0]).toBe("Travel Emergency");
  });
});
